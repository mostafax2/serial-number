<?php

/**
 * 
 * Mostafa Elbayyar
 * mostafa.m.elbiar@gmail.com
 * +201002747846
 */
class Serial
{
	/*Get Sum [Serial Number] return integer {$sum} */
	public static function getSumSerialNumber($key)
	{
		$serialKey = str_split($key);
		$sum = 0;
		for ($i = 0; $i < count($serialKey); $i++) {
			$mul = 0;
			$num = ord($serialKey[$i]);
			if ($i % 2 == 1) {
				$mul = (string)($num * 2);

				if (strlen($mul) == 2) {
					$a = intval($mul[0]);
					$b = intval($mul[1]);
					$num = $a + $b;
				} elseif (strlen($mul) == 3) {
					$a = intval($mul[0]);
					$b = intval($mul[1]);
					$c = intval($mul[2]);
					$num = $a + $b + $c;
				} else {
					$num = $mul;
				}
				$sum = $sum + $num;
			} else {
				$sum = $sum + $num;
			}
		}
		return $sum;
	}

	public static function genrateSerialNumber()
	{
		$seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'); // and any other characters
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, 10) as $k) {
			$rand .= $seed[$k];
		}
		return $rand;
	}
}
