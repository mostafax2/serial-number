<?php

/**
 * 
 * Mostafa Elbayyar
 * mostafa.m.elbiar@gmail.com
 * +201002747846
 */

use function PasswordCompat\binary\check;

date_default_timezone_set('Asia/Kuwait');
require_once('serial.php');


$serial = Serial::genrateSerialNumber();
$sum = Serial::getSumSerialNumber($serial);
$serialnumber = $serial . " " . ($sum + 2) . " " . time();
echo "S/N : (" . $serialnumber . ")<br>"; 
echo "<hr>";


/* check serial number is true */
$serial = explode(" ", $serialnumber);
$Key = $serial[0];
$check = $serial[1];
$timeRequest = $serial[2];
$sum = Serial::getSumSerialNumber($Key);


// check key
$checkKey = (($sum + 2) == $check) ? 1 : 0 ;
// check time request 
$checkTime = (time() <= ($timeRequest+10) and time() > $timeRequest-1) ? 1 : 0 ;


if ($checkTime and $checkKey) {
    echo "SERIAL NUMBER [" . $serialnumber . "] is  Success"; " and Time Request is Success ";
}else{
    echo "Sorry! ";
}

echo "<br> Time Request ".$timeRequest." : ". date('d/m/Y H:i:s', $timeRequest+30);
echo "<br> Time Now ".time().": ". date('d/m/Y H:i:s', (time()));

